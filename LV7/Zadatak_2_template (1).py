import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as Image
from sklearn.cluster import KMeans

# ucitaj sliku
for i in range(1,4):
    img = Image.imread(f"imgs\\test_{i}.jpg")

    # prikazi originalnu sliku
    plt.figure()
    plt.title("Originalna slika")
    plt.imshow(img)
    plt.tight_layout()
    plt.show()

    # pretvori vrijednosti elemenata slike u raspon 0 do 1
    img = img.astype(np.float64) / 255

    # transfromiraj sliku u 2D numpy polje (jedan red su RGB komponente elementa slike)
    w,h,d = img.shape
    img_array = np.reshape(img, (w*h, d))
    unique_colors = np.unique(img_array, axis=0)
    num_unique_colors = len(unique_colors)
    print("Broj različitih boja u slici:", num_unique_colors)

    # rezultatna slika
    img_array_aprox = img_array.copy()
    km = KMeans(n_clusters =5 , init ='random',n_init =5 ,random_state =0)
    km.fit(img_array_aprox)
    labels=km.labels_
    centroids=km.cluster_centers_
    for i in range(len(centroids)):
         img_array_aprox[labels==i]=centroids[i]
    img_aprox = np.reshape(img_array_aprox, (w, h, d))
    plt.imshow(img_aprox)
    plt.axis('off')
    plt.title('Aproksimirana slika')
    plt.show()
    


    inertia = []
    k_range = range(1, 11)

    # Izračunavanje inercije za svaku vrednost K
    for k in k_range:
        kmeans = KMeans(n_clusters=k, random_state=0,n_init=5)
        kmeans.fit(img_array)
        inertia.append(kmeans.inertia_)

    # Prikazivanje rezultata na grafikonu
    plt.figure(figsize=(8, 6))
    plt.plot(k_range, inertia, marker='o', linestyle='-')
    plt.xlabel('Broj grupa K')
    plt.ylabel('Inercija')
    plt.title('Zavisnost inercije od broja grupa K')
    plt.grid(True)
    plt.show()

    K=2
    for i in range(K):
        binary_img=np.zeros((w*h,d))
        binary_img[labels==i]=1
        binary_img=np.reshape(binary_img,(w,h,d))
        plt.figure()
        plt.imshow(binary_img,cmap="gray")
        plt.title(f"Grupa{i+1}")
        plt.axis("off")
        plt.show()


 
#4 Povečanjem broja klastera, poveča se i kvaliteta slike(dobijemo više boja, jer svaki klaster predstavlja jednu boju)
#7 Svaka binarna slika prikazuje samo jednu boju (jedan centroid). Koliko ima klastera toliko ima i centroida i boja. Boja na slici je označena crnom bojom
#dok su ostale boje prikazane bijelom.