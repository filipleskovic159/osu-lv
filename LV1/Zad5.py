
total_ham=[]
total_spam=[]
ends_with_ex=0
fhand = open('SMSSpamCollection.txt')
for line in fhand :
    line = line.rstrip()
    words = line.split()
    if words[0]=="ham":
        total_ham.append(len(words)-1)
    elif words[0]=="spam":
        total_spam.append(len(words)-1)
        if words[-1] == "!" or words[-1].endswith("!"):
            ends_with_ex+=1

print(sum(total_ham)/len(total_ham))
print(sum(total_spam)/len(total_spam))
print(ends_with_ex)