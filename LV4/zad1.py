import pandas as pd
import numpy as np
import math as mt
import matplotlib.pyplot as plt
from sklearn . model_selection import train_test_split
from sklearn . preprocessing import MinMaxScaler
import sklearn . linear_model as lm
from sklearn . metrics import mean_absolute_error,mean_absolute_percentage_error,mean_squared_error,r2_score

data=pd.read_csv('data_C02_emission.csv')
input_var= ['Fuel Consumption City (L/100km)',
            'Fuel Consumption Hwy (L/100km)',
            'Fuel Consumption Comb (L/100km)',
            'Fuel Consumption Comb (mpg)',
            'Engine Size (L)',
            'Cylinders']
output=['CO2 Emissions (g/km)']
data=data.drop(['Make','Model','Vehicle Class'],axis=1)
X=data[input_var].to_numpy()
y=data[output].to_numpy()
X_train , X_test , y_train , y_test = train_test_split(X,y,test_size=0.1 , random_state =1 )

#b
plt.figure()
plt.scatter(x=X_train[:,4],y=y_train,c='red',label='Train data',s=52)
plt.scatter(x=X_test[:,4],y=y_test,c='blue',label='Test data',s=15)
plt.xlabel('Engine Size (L)')
plt.ylabel('CO2 Emissons (g/km)')
plt.legend()
plt.show()

#c
plt.figure()
plt.subplot(2,1,1)
plt.hist(x=X_train[:,1],bins=10)
sc = MinMaxScaler ()
X_train_n = sc.fit_transform(X_train )
X_test_n = sc.transform(X_test)
plt.subplot(2,1,2)
plt.hist(x=X_train_n[:,1],bins=10)
plt.show()


#d
linearModel= lm.LinearRegression ()
linearModel.fit(X_train_n , y_train)
print(linearModel.coef_)

#e

y_p=linearModel.predict(X_test_n)
plt.figure()
plt.scatter(x=X_test_n[:,4],y=y_test,c='red',s=20,label='real')
plt.scatter(x=X_test_n[:,4],y=y_p,c='blue',s=20,label='predicted')
plt.xlabel('Engine Size (L)')
plt.ylabel('CO2 Emission')
plt.legend()
plt.show()

#f

MAE=mean_absolute_error(y_test,y_p)
MSE=mean_squared_error(y_test,y_p)
MAPE=mean_absolute_percentage_error(y_test,y_p)
R2=r2_score(y_test,y_p)
print(MSE)
print(mt.sqrt(MSE))
print(MAE)
print(MAPE)
print(R2)

#g Promjenom broja ulaznih veličina mjenjaju se i vrijednosti metrika. Npr povečanjem train podataka dobije se manji MSE, RMSE, ne prevelik ali ipak model točnije pokazuje podatke.






