import pandas as pd
import numpy as np
import math as mt
import matplotlib.pyplot as plt
from sklearn . model_selection import train_test_split
from sklearn . preprocessing import MinMaxScaler,OneHotEncoder
import sklearn . linear_model as lm
from sklearn . metrics import mean_absolute_error,mean_absolute_percentage_error,mean_squared_error,r2_score

data = pd.read_csv("data_C02_emission.csv")

ohe = OneHotEncoder()
X_encoded = ohe.fit_transform(data[["Fuel Type"]]).toarray()
ohe_columns = ohe.get_feature_names_out(["Fuel Type"])

ohe_df = pd.DataFrame(columns=ohe_columns, data=X_encoded,index=data.index)
new_data = pd.concat([data, ohe_df], axis=1)
new_data = new_data.drop(columns=["Fuel Type"], axis=1)

print(new_data)

input_var = [
"Engine Size (L)",
"Cylinders",
"Fuel Consumption City (L/100km)",
"Fuel Consumption Hwy (L/100km)",
"Fuel Consumption Comb (L/100km)",
"Fuel Consumption Comb (mpg)",
"Fuel Type_D",
"Fuel Type_E",
"Fuel Type_X",
"Fuel Type_Z",
]
output = ["CO2 Emissions (g/km)"]

X=new_data[input_var].to_numpy()
y=new_data[output].to_numpy()
X_train , X_test , y_train , y_test = train_test_split(X,y,test_size=0.1 , random_state =1 )
linearmodel= lm.LinearRegression()
linearmodel.fit(X_train,y_train)

y_p=linearmodel.predict(X_test)
abs_errors=abs(y_test-y_p)
max_error_index=abs_errors.argmax()
max_error=abs_errors[max_error_index]

data = pd.read_csv("data_C02_emission.csv")

vehicle_model=data.loc[max_error_index,'Model']
print(f"Max error je : {max_error}")
print(f"Model:{vehicle_model}")