import numpy as np
import matplotlib.pyplot as plt

data = []
with open('data.csv', 'r') as fhand:
    next(fhand)
    for line in fhand:
        words = line.strip().split(',')  
        row = [float(word) for word in words]
        data.append(row)
data = np.array(data)
print(len(data))

height=data[:,1]
weight=data[:,2]
height=np.array(height)
weight=np.array(weight)
print(np.max(height))
print(np.min(height))
print(np.mean(height))
plt.scatter(height,weight,alpha=0.5)
plt.title('Odnos težine i visine svih osoba')
plt.show()


height=[]
weight=[]
for i in range (0,len(data),50):
    height.append(data[i,1])
    weight.append(data[i,2])
plt.scatter(height,weight,alpha=0.5)
plt.title('Odnos težine i visine svakih 50 osoba')
plt.show()

mheight=[]
wheight=[]
for i in range (len(data)):
    if data[i,0]: 
        mheight.append(data[i,1])
    else:
        wheight.append(data[i,1])
print('Muskarci')
print(np.max(mheight))
print(np.min(mheight))
print(np.mean(mheight))
print('Zene')
print(np.max(wheight))
print(np.min(wheight))
print(np.mean(wheight))




