import numpy as np
import matplotlib . pyplot as plt

zeros=np.zeros((50,50))
ones=np.ones((50,50))
up=np.hstack((zeros,ones))
down=np.hstack((ones,zeros))
picture=np.vstack((up,down))

plt.figure()
plt.imshow(picture,cmap="gray")
plt.title('Crno bijela slika')
plt.show()