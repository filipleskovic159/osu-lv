import numpy as np
import matplotlib . pyplot as plt


img = plt.imread ("road.jpg")
img = img [ :,:,0]. copy ()
brightness = 30
rotated=np.rot90(img,-1)
cropped = img[:img.shape[0],img.shape[1]//4:img.shape[1]//2]
inverted=np.fliplr(img)
inverted_v=np.flipud(img)
new_img=img+150
new_img[new_img>255]=255
new_img[new_img<150]=255
plt.figure()
plt.subplot(3, 2, 1)
plt.imshow(img,cmap="gray")
plt.title('Originalna slika')

plt.subplot(3, 2, 2)
plt.imshow(new_img,cmap="gray")
plt.title('Posvijetljena slika')

plt.subplot(3, 2, 3)
plt.imshow(rotated,cmap="gray")
plt.title('Zarotirana slika')

plt.subplot(3, 2, 4)
plt.imshow(cropped,cmap="gray")
plt.title('Izrezana slika')

plt.subplot(3, 2, 5)
plt.imshow(inverted,cmap="gray")
plt.title('Invertirana slika')

plt.subplot(3, 2, 6)
plt.imshow(inverted_v,cmap="gray")
plt.title('Invertirana vert slika')
plt.subplots_adjust(hspace=0.9)
plt.show()

