import numpy as np
import matplotlib
import matplotlib.pyplot as plt

from sklearn.datasets import make_classification
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import confusion_matrix, accuracy_score, precision_score, recall_score


X, y = make_classification(n_samples=200, n_features=2, n_redundant=0, n_informative=2,
                            random_state=213, n_clusters_per_class=1, class_sep=1)

# train test split
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=5)

# Plot podataka za učenje
plt.figure(figsize=(10, 5))
plt.scatter(X_train[:, 0], X_train[:, 1], c=y_train, marker='o', cmap='viridis', label='Train data')
# Plot podataka za testiranje
plt.scatter(X_test[:, 0], X_test[:, 1], c=y_test, marker='x', cmap='viridis', label='Test data')
plt.xlabel('X1')
plt.ylabel('X2')
plt.title('Podaci za učenje i testiranje')
plt.legend()
plt.show()
#b
LogRegression_model = LogisticRegression()
LogRegression_model.fit(X_train , y_train)

#c
theta1, theta2 = LogRegression_model.coef_[0]
theta0 = LogRegression_model.intercept_

plt.figure(figsize=(10, 5))
plt.scatter(X[:, 0], X[:, 1], c=y, marker='o', cmap='viridis', label='Train data')

x1_values = np.linspace(min(X[:, 0]), max(X[:, 0]), 100)
x2_values = -(theta0 + theta1 * x1_values) / theta2
plt.plot(x1_values, x2_values, color='red', label='Decision Boundary')

plt.xlabel('X1')
plt.ylabel('X2')
plt.title('Granica odluke naučenog modela Logistic Regression')
plt.legend()
plt.show()

#d
y_pred=LogRegression_model.predict(X_test)
conf_matrix = confusion_matrix(y_test, y_pred)
print("Matrica zabune:")
print(conf_matrix)

# Izračun točnosti
accuracy = accuracy_score(y_test, y_pred)
print("Točnost:", accuracy)

# Izračun preciznosti
precision = precision_score(y_test, y_pred)
print("Preciznost:", precision)

# Izračun odziva
recall = recall_score(y_test, y_pred)
print("Odziv:", recall)

#e
correctly_classified = X_test[y_pred == y_test]
misclassified = X_test[y_pred != y_test]
plt.figure(figsize=(10, 5))
plt.scatter(correctly_classified[:, 0], correctly_classified[:, 1], c='green', marker='o', label='Correctly classified')
plt.scatter(misclassified[:, 0], misclassified[:, 1], c='black', marker='o', label='Misclassified')
plt.xlabel('X1')
plt.ylabel('X2')
plt.title('Skup za testiranje')
plt.legend()
plt.show()
