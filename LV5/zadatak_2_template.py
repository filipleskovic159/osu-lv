import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.colors import ListedColormap
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import confusion_matrix, accuracy_score, classification_report
import seaborn as sns

labels= {0:'Adelie', 1:'Chinstrap', 2:'Gentoo'}

def plot_decision_regions(X, y, classifier, resolution=0.02):
    plt.figure()
    # setup marker generator and color map
    markers = ('s', 'x', 'o', '^', 'v')
    colors = ('red', 'blue', 'lightgreen', 'gray', 'cyan')
    cmap = ListedColormap(colors[:len(np.unique(y))])
    
    # plot the decision surface
    x1_min, x1_max = X[:, 0].min() - 1, X[:, 0].max() + 1
    x2_min, x2_max = X[:, 1].min() - 1, X[:, 1].max() + 1
    xx1, xx2= np.meshgrid(np.arange(x1_min, x1_max, resolution),
    np.arange(x2_min, x2_max, resolution))
    Z = classifier.predict(np.array([xx1.ravel(), xx2.ravel()]).T)
    Z = Z.reshape(xx1.shape)
    plt.contourf(xx1, xx2, Z, alpha=0.3, cmap=cmap)
    plt.xlim(xx1.min(), xx1.max())
    plt.ylim(xx2.min(), xx2.max())
    
    # plot class examples
    for idx, cl in enumerate(np.unique(y)):
        plt.scatter(x=X[y == cl, 0],
                    y=X[y == cl, 1],
                    alpha=0.8,
                    c=colors[idx],
                    marker=markers[idx],
                    edgecolor = 'w',
                    label=labels[cl])

# ucitaj podatke
df = pd.read_csv("penguins.csv")

# izostale vrijednosti po stupcima
print(df.isnull().sum())

# spol ima 11 izostalih vrijednosti; izbacit cemo ovaj stupac
df = df.drop(columns=['sex'])

# obrisi redove s izostalim vrijednostima
df.dropna(axis=0, inplace=True)

# kategoricka varijabla vrsta - kodiranje
df['species'].replace({'Adelie' : 0,
                        'Chinstrap' : 1,
                        'Gentoo': 2}, inplace = True)

print(df.info())

# izlazna velicina: species
output_variable = ['species']

# ulazne velicine: bill length, flipper_length
input_variables = ['bill_length_mm',
                    'flipper_length_mm',]

X = df[input_variables].to_numpy()
y = df[output_variable].to_numpy()

# podjela train/test
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.2, random_state = 123)

#a
train_classes, train_counts = np.unique(y_train, return_counts=True)

# Broj primjera za svaku klasu u skupu podataka za testiranje
test_classes, test_counts = np.unique(y_test, return_counts=True)

# Prikazujemo klasu umjesto broja
train_classes = [labels[c] for c in train_classes]
test_classes = [labels[c] for c in test_classes]

# Stupčasti dijagram za skup za učenje
plt.figure(figsize=(10, 5))
plt.bar(train_classes, train_counts, color='blue', alpha=0.6, label='Train data')

# Stupčasti dijagram za skup za testiranje
plt.bar(test_classes, test_counts, color='red', alpha=0.6, label='Test data')

plt.xlabel('Klasa')
plt.ylabel('Broj primjera')
plt.title('Broj primjera za svaku klasu u skupu za učenje i testiranje')
plt.legend()
plt.show()

#b
LogRegression_model = LogisticRegression()
LogRegression_model.fit(X_train , y_train)

#c
coefs=LogRegression_model.coef_
intercepts=LogRegression_model.intercept_
for i, label in labels.items():
    print(f"Koeficijenti za klasu '{label}': {coefs[i]}")
for i, label in labels.items():
    print(f"Odsječak za klasu '{label}': {intercepts[i]}")
    
plot_decision_regions(X_train,y_train.ravel(),LogRegression_model)
plt.xlabel('X1')
plt.ylabel('X2')
plt.title('Granice odluke modela')
plt.legend()
plt.show()

y_pred=LogRegression_model.predict(X_test)
# Izračun matrice zabune
conf_matrix = confusion_matrix(y_test, y_pred)
plt.figure(figsize=(8, 6))
sns.heatmap(conf_matrix, annot=True, fmt="d", cmap="Blues")
plt.xlabel('Predicted labels')
plt.ylabel('True labels')
plt.title('Confusion Matrix')
plt.show()

# Izračun točnosti
accuracy = accuracy_score(y_test, y_pred)
print("Točnost:", accuracy)

# Ispis glavnih metrika
print("Izvještaj klasifikacije:")
print(classification_report(y_test, y_pred))