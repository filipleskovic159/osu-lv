import pandas as pd
import numpy as np
data = pd.read_csv ('data_C02_emission.csv')
object_columns=data.select_dtypes(include=['object']).columns
data[object_columns]=data[object_columns].astype('category')
print(data.info())
if(data.duplicated().sum()>0):
    data.drop_duplicates()
print(data.info())

