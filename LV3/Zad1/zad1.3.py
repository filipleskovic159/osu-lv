import pandas as pd
import numpy as np
data = pd.read_csv ('data_C02_emission.csv')
object_columns=data.select_dtypes(include=['object']).columns
data[object_columns]=data[object_columns].astype('category')

engine_size=data[(data['Engine Size (L)'] >= 2.5 ) & ( data['Engine Size (L)']<=3.5)]
print('Automobili koji imaju motore izmedju 2.5 i 3.5 L')
print(len(engine_size))
print(engine_size['CO2 Emissions (g/km)'].mean())