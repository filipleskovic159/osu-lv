import pandas as pd
import numpy as np
data = pd.read_csv ('data_C02_emission.csv')
object_columns=data.select_dtypes(include=['object']).columns
data[object_columns]=data[object_columns].astype('category')
car=data[(data['Cylinders'] ==4 ) & ( data['Fuel Type']=='D')].sort_values(by=['Fuel Consumption City (L/100km)'],ascending=False)
print(car.head(1))