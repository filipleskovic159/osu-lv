import pandas as pd
import numpy as np
data = pd.read_csv ('data_C02_emission.csv')
object_columns=data.select_dtypes(include=['object']).columns
data[object_columns]=data[object_columns].astype('category')

cylinders_2_and_even=data[(data['Cylinders']>2 )& (data['Cylinders']%2==0)]
print('Broj auta po cilindru i prosječna emsiija plinova s obzirom na broj cilindara')
data_group_by_cylinders=cylinders_2_and_even.groupby('Cylinders')
print(data_group_by_cylinders.count())
print(data_group_by_cylinders['CO2 Emissions (g/km)'].mean())