import pandas as pd
import numpy as np
data = pd.read_csv ('data_C02_emission.csv')
object_columns=data.select_dtypes(include=['object']).columns
data[object_columns]=data[object_columns].astype('category')

ekstremi_potrosnje=data[['Make','Model','Fuel Consumption City (L/100km)']].sort_values(by=['Fuel Consumption City (L/100km)'],ascending=False) 
print("Najveca i najmanja potrosnja:")
print(ekstremi_potrosnje.head(3))
print(ekstremi_potrosnje.tail(3))
