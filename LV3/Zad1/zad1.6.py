import pandas as pd
import numpy as np
data = pd.read_csv ('data_C02_emission.csv')
object_columns=data.select_dtypes(include=['object']).columns
data[object_columns]=data[object_columns].astype('category')

diesel_cars=data[(data['Fuel Type'])=='D']
print('Prosjecna gradska potrosnja i medijan za dizel:')
print(diesel_cars['Fuel Consumption City (L/100km)'].mean())
print(diesel_cars['Fuel Consumption City (L/100km)'].median())
regularb_cars=data[(data['Fuel Type'])=='X']
print('Prosjecna gradska potrosnja i medijan za regularni benzin:')
print(regularb_cars['Fuel Consumption City (L/100km)'].mean())
print(regularb_cars['Fuel Consumption City (L/100km)'].median())