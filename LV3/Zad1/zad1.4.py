import pandas as pd
import numpy as np
data = pd.read_csv ('data_C02_emission.csv')
object_columns=data.select_dtypes(include=['object']).columns
data[object_columns]=data[object_columns].astype('category')

audi_cars=data[(data['Make'])=='Audi']
print('Broj automobila prozivednih od Audia')
print(len(audi_cars))
audi_with_4_cylinders=audi_cars[(audi_cars['Cylinders']==4)]
print('Emisija CO2 Audija s 4 cilindara')
print(audi_with_4_cylinders['CO2 Emissions (g/km)'].mean())