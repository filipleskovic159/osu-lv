import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches

data = pd.read_csv ('data_C02_emission.csv')
colors = {
    'X': 'red',
    'Z': 'blue',
    'D': 'orange',
    'E': 'yellow',
    'N': 'green'
}
plt.scatter(x=data['CO2 Emissions (g/km)'],y=data['Fuel Consumption City (L/100km)'],c=data['Fuel Type'].map(colors),s=30,label=data['Fuel Type'])

legend_handles = [mpatches.Patch(color=color, label=fuel_type) for fuel_type, color in colors.items()]
plt.legend(handles=legend_handles, title='Fuel Type', loc='best')

plt.xlabel('CO2 Emissions (g/km)')
plt.ylabel('Fuel Consumption City (L/100km)')
plt.show()  