import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

data = pd.read_csv ('data_C02_emission.csv')
object_columns=data.select_dtypes(include=['object']).columns
data[object_columns]=data[object_columns].astype('category')
plt.figure()
data['CO2 Emissions (g/km)'].plot( kind ='hist', bins = 20)

plt.xlabel('Raspon automobila')
plt.ylabel('CO2 Emission (g/km)')
plt.show()
