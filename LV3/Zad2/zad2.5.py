import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
data = pd.read_csv ('data_C02_emission.csv')
cars_by_cylinders=data.groupby('Cylinders')['CO2 Emissions (g/km)'].mean()
cars_by_cylinders.plot(kind='bar',color='blue')
plt.show()